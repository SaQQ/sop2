/**
 * \brief Synchronous multiplexing utilizing pselect() (with signal waiting)
 */

#define _GNU_SOURCE

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

struct sockaddr_in make_address(const char *address, const char *port) {
  int ret;
  struct sockaddr_in addr;
  struct addrinfo *result;
  struct addrinfo hints = {};
  hints.ai_family = AF_INET;
  if ((ret = getaddrinfo(address, port, &hints, &result))) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
    exit(EXIT_FAILURE);
  }
  addr = *(struct sockaddr_in *)(result->ai_addr);
  freeaddrinfo(result);
  return addr;
}

#define MAX_CLIENTS 3

int set_nonblock(int desc) {
  int oldflags = fcntl(desc, F_GETFL, 0);
  if (oldflags == -1)
    return -1;
  oldflags |= O_NONBLOCK;
  return fcntl(desc, F_SETFL, oldflags);
}

volatile sig_atomic_t interrupted = 0;

void signal_handler(int sig) { interrupted = 1; }

void do_server(int server_socket) {

  struct sigaction sa;
  memset(&sa, 0, sizeof(sa));
  sa.sa_handler = signal_handler;
  if (sigaction(SIGINT, &sa, NULL) < 0) {
    perror("sigaction(): ");
    exit(EXIT_FAILURE);
  }

  sigset_t sigmask, old_sigmask;
  sigemptyset(&sigmask);
  sigaddset(&sigmask, SIGINT);
  sigprocmask(SIG_BLOCK, &sigmask, &old_sigmask);

  struct pollfd clients[MAX_CLIENTS];
  for (int i = 0; i < MAX_CLIENTS; ++i)
    clients[i].fd = -1;
  clients[0].fd = server_socket;
  clients[0].events = POLLIN;

  while (1) {
    printf("Calling ppoll()!\n");
    int ret = ppoll(clients, MAX_CLIENTS, NULL, &old_sigmask);
    if (ret < 0) {
      if (errno == EINTR) {
        if (interrupted)
          break;
      }
      perror("select()");
      exit(EXIT_FAILURE);
    }

    for (int i = 0; i < MAX_CLIENTS; ++i) {

      if (clients[i].fd == server_socket) {

        if (clients[i].revents & POLLIN) {

          struct sockaddr client_addr;
          socklen_t client_addrlen = sizeof(client_addr);
          int client = accept(server_socket, &client_addr, &client_addrlen);

          if (client >= 0) {
            printf("Accepted new client!\n");
            set_nonblock(client);
            for (int i = 0; i < MAX_CLIENTS; ++i) {
              if (clients[i].fd == -1) {
                clients[i].fd = client;
                clients[i].events = POLLIN;
                break;
              } else if (i == MAX_CLIENTS - 1) {
                char neat_reply[] = "No space for you :(\n";
                send(client, neat_reply, sizeof(neat_reply), 0);
                close(client);
              }
            }
          } else if (errno != EAGAIN) {
            perror("accept()");
            exit(EXIT_FAILURE);
          }
        }
      } else {

        if (clients[i].revents & POLLIN) {
          char buff[16];
          ssize_t ret = recv(clients[i].fd, buff, sizeof(buff), 0);
          printf("recv returned %ld bytes\n", ret);
          if (ret > 0) {
            printf("Client sent: '%.*s'\n", (int)ret, buff);
          } else if (ret == 0) {
            close(clients[i].fd);
            clients[i].fd = -1;
          } else if (errno != EAGAIN) {
            perror("recv()");
            exit(EXIT_FAILURE);
          }
        }
      }
    }
  }

  for (int i = 0; i < MAX_CLIENTS; ++i) {
    if (clients[i].fd >= 0) {
      close(clients[i].fd);
    }
  }
}

int main(int argc, const char *argv[]) {

  if (argc != 3) {
    fprintf(stderr, "USAGE: %s host port\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in addr = make_address(argv[1], argv[2]);
  int server_socket = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
  if (server_socket < 0) {
    perror("socket()");
    exit(EXIT_FAILURE);
  }

  if (bind(server_socket, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    perror("bind()");
    exit(EXIT_FAILURE);
  }

  if (listen(server_socket, 3) < 0) {
    perror("listen()");
    exit(EXIT_FAILURE);
  }

  do_server(server_socket);

  close(server_socket);
  return 0;
}
