## TCP/IP Networking

Operating Systems 2 - Lecture 5

-----

Obtain addresses via `ip` command:
```shell
ip address
ip a
```

Example:

```
[...]
2: wlp0s20f3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether dc:21:5c:08:bf:83 brd ff:ff:ff:ff:ff:ff
    inet 192.168.178.28/24 brd 192.168.178.255 scope global dynamic noprefixroute wlp0s20f3
       valid_lft 863466sec preferred_lft 863466sec
    inet6 fe80::2ecb:209a:b7d2:bd77/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
[...]
```

Interface `wlp0s20f3` has MAC address `dc:21:5c:08:bf:83` and IP addresses `192.168.178.28` and `fe80::2ecb:209a:b7d2:bd77` assigned.

### Encapsulation

##### Setup

Start wireshark capture on used interface.
```shell
sudo systemd-resolve --flush-caches
```

Try to connect to a different host while having a Wireshark capture enabled:

```shell
telnet pw.edu.pl 80
```

Look into packets in wireshark and determine where it has taken
all the info from.

ARP flush:

```shell
# Start wireshark capture on `docker0`
sudo ip neigh flush all
ip neigh
```

Routing table:
```shell
ip route
```

### IP

Fragmentation:

```shell
cat /dev/urandom | head -c 8000 | nc -u pw.edu.pl 80 
```


### TCP
    
Demonstrate netcat:

```shell
# Setup server listening on 8080
nc -l 8080
```

```shell
nc localhost 8080
```

Close connection by the client and the server.

Flow control:

```shell
# Setup SLOW server listening on 8080
nc -l 8080 | sleep 60
```

```shell
# flood it with data
cat /dev/urandom | nc localhost 8080
```


### Address conversions

```shell
gcc 02_addr.c -o /tmp/02_addr -g
/tmp/02_addr 127.0.0.1
```

```shell
/tmp/02_addr asdf
```

```shell
/tmp/02_addr 255.255.255.255
```

```shell
gcc 03_pton.c -o /tmp/03_pton -g
/tmp/03_pton 127.0.0.1
```

```shell
/tmp/03_pton 255.255.255.255
```

### DNS resolution

```shell
gcc 04_getaddrinfo.c -o /tmp/04_getaddrinfo -g
```

```shell
/tmp/04_getaddrinfo localhost
```

```shell
/tmp/04_getaddrinfo localhost 22
```

```shell
/tmp/04_getaddrinfo localhost ssh
```

Run under wireshark with `dns` filter:

```shell
/tmp/04_getaddrinfo google.pl http
```

### UDP and socket addressing

```shell
gcc 05_udp.c -o /tmp/05_udp -g
/tmp/05_udp localhost 1234
```

```shell
gcc 06_udp_eph.c -o /tmp/06_udp_eph -g
/tmp/06_udp_eph
```

### TCP

```shell
gcc 07_tcp.c -o /tmp/07_tcp -g
/tmp/07_tcp localhost 1234
```

```shell
gcc 08_tcp_forked.c -o /tmp/08_tcp_forked -g
/tmp/08_tcp_forked localhost 1234
```

```shell
# Run in case of address already in use
ss -tan
```

```shell
gcc 09_tcp_epipe.c -o /tmp/09_tcp_epipe -g
/tmp/09_tcp_epipe localhost 1234
```

### select, poll, epoll

```shell
gcc 10_select.c -o /tmp/10_select -g
/tmp/10_select localhost 1234
```

```shell
gcc 15_pselect.c -o /tmp/15_pselect -g
/tmp/15_pselect localhost 1234
```

```shell
gcc 20_poll.c -o /tmp/20_poll -g
/tmp/20_poll localhost 1234
```

```shell
gcc 30_epoll.c -o /tmp/30_epoll -g
/tmp/30_epoll localhost 1234
```

